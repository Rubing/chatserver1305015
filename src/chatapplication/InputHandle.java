package chatapplication;

import java.io.PrintWriter;
import java.util.ArrayList;

public class InputHandle implements Observer{
    private ArrayList<PrintWriter> robots;
    private History chatHistory;
    private PrintWriter out;
    
    public InputHandle(PrintWriter out, History chatHistory) {
        this.chatHistory = chatHistory;
        this.out= out;
        this.chatHistory.AddListerners(this);
        
    }
    
    
    public void userContent (String content){
//            replace String later
        
        chatHistory.store(content);
        
        
    }
    
    
    public void deliveryContent(String content) {
        
        out.println(content);
        out.flush();
    }
    
    
}


