
package chatapplication;

public interface Observer {
    
    void deliveryContent (String content); 
    
}
