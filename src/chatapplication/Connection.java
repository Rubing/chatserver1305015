package chatapplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Connection implements Runnable {
    
    private PrintWriter out;
    private BufferedReader in;
    private String userName;
    private InputHandle userInput;
    
    public Connection (Socket uniqueSocket, History chatHistory) throws IOException {
        
        out = new PrintWriter(uniqueSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(uniqueSocket.getInputStream()));
        this.userName = uniqueSocket.getInetAddress().toString() + " - " + uniqueSocket.getPort();
//        userInput.addUser(userName, out);
//        addUser should be replace by the xinput handler constructor
        userInput = new InputHandle(out, chatHistory);
        out.println("Welcome to the chat server!");
        System.out.println("New connection: " + this.userName);
    }
    
    @Override
    public void run( ) {
        
        while(true){
            
            try {
                String content = in.readLine();
                if (content == null) {
                    System.out.println(userName + " left");
                    break;
                }
                if (content.indexOf("/nick") == 0 && content.length() > 6) {
                    String oldName = this.userName;
                    this.userName = content.substring(6);
                    content = oldName + " is now known as " + this.userName;
                }
                userInput.userContent(this.userName + ": " + content);
                
            } catch (IOException ex) {
                Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    
    
}


