package chatapplication;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChatApplication {
    
    public static void main(String[] args) {
        
        History chatHistory = new History();
        
        try {
            
            ServerSocket firstSocket = new ServerSocket(8777);
            
//            System.out.println(firstSocket.getInetAddress());
            System.out.println(firstSocket.getLocalPort());
            
            
            System.out.println("New connection is built. ");
            
            
            
            while(true){
                
                Socket uniqueSocket = firstSocket.accept();
                Connection newConnection = new Connection(uniqueSocket, chatHistory);
                Thread tt = new Thread(newConnection);
                tt.start();
            }
            
        } catch (IOException ex) {
            Logger.getLogger(ChatApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
