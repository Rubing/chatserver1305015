
package chatapplication;

import java.util.ArrayList;

public class History {
    
    private ArrayList<String> chatHistory = new ArrayList<>();
    private ArrayList<Observer> participants = new ArrayList<>();
    
    public History (){
        
    }
    
    public void store (String singleMSG){
        chatHistory.add(singleMSG);
        
        for (Observer oo: participants){
            oo.deliveryContent(singleMSG);
        }
        
    }
    public void AddListerners (Observer singleUser) {
        participants.add(singleUser);
    }
    
    
    
}
